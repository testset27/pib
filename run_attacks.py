import matplotlib.pyplot as plt 

import argparse
import sys

from tensorflow.examples.tutorials.mnist import input_data

import tensorflow as tf
import numpy as np

from test_attack_pib import *
from config import cfg 
from model import * 

from l2_attack_pib import CarliniL2_pib
import tools.inspect_checkpoint as inspect_checkpoint

model = 'greedypib' #'greedypib'

# FOR GREEDYPIB ONLY 
# NETWORK 
cfg.in_dim = 784
cfg.out_dim = 10
cfg.hidden_dims = [512,512]#[1024, 1024]
cfg.output_structure = 'categorical' 
#'multivariate_ber', 'univariate_ber', 'categorical', 'multivariate_normal'
cfg.activation = 'sigmoid'

# IF STOCHASTICITY 
cfg.non_binary = False #False

# LOSS TYPE 
cfg.loss_type = 'pib' #nll, vcr, pib 

# STOCHASTICITY HYPERPARAMETERS
cfg.train_n_samples = [32,32]
cfg.test_n_samples = [32,32]
cfg.beta = 1e-4
cfg.estimator = 'Raiko'
cfg.seed = 1111

cfg.max_num_epochs = 1000
cfg.optimizer_arg = {'adadelta':{'learning_rate':1, 'rho': 0.95, 'epsilon':1e-6}, 
            'adam':{'learning_rate':1e-4, 'beta1':0.9, 'beta2':0.999, 'epsilon':1e-8},
            'gradientdescent':{'learning_rate':1},
            'adagrad':{'learning_rate':0.1}}
cfg.optimizer = 'adadelta'
cfg.display_freq = 100#None 
cfg.save_freq = 100#None
cfg.batch_size = 32 #16
cfg.test_batch_size = 100 #16
cfg.valid_freq = 400#None

cfg.pretrained_stage = 2 # range [1,L]. If i, train w_i and load w_{<i} 

cfg.logdir = './log/greedy_pib/beta4/seed1111/adadelta/lr1/val'
cfg.load_pretrained_from = os.path.join(cfg.logdir, 'retrained_layer%d'%(cfg.pretrained_stage-1))
cfg.save_trained_to = os.path.join(cfg.logdir, 'retrained_layer%d'%(cfg.pretrained_stage))

cfg.logdir = os.path.join(cfg.logdir, 'l%d'%(cfg.pretrained_stage)) 
cfg.bestdir = os.path.join(cfg.logdir, 'best')




TARGETED = True
BATCH_SIZE = 10
SAMPLES = 1000
target_type = 'all' #'zero2one'

print(cfg.bestdir)

defense, success_attack, shifted,\
inputs, labels, adv,defense_rate, success_attack_rate, \
mags_of_success_attacks, mags_of_success_defense, mags_of_shifted = \
        evaluate_attack(targeted=TARGETED, target_type=target_type, samples = SAMPLES,\
                        model = model,
                        batch_size=BATCH_SIZE, NON_RAND=False, best_or_latest='best', GPU=2)