from utils import _makedir
import os 

class cfg():
    # MODEL
    model = 'pib' #pib, vib

    # DATASET
    dataset = 'cifar10'#'cifar10' #'tfd' #'mnist'

    # MODE: Train or test
    VAL = True
    show_test_every_epoch = False

    # NETWORK 
    in_dim = 3072#3072#2304#784 
    out_dim = 10#10#2304#10 
    hidden_dims = [512,512]#[1024, 1024]
    output_structure = 'categorical' #'multivariate_ber', 'univariate_ber', 'categorical', 'multivariate_normal'
    activation = 'sigmoid'

    # IF STOCHASTICITY 
    non_binary = False #False #False

    # LOSS TYPE 
    loss_type = 'nll' #nll, vcr, pib 

    # STOCHASTICITY HYPERPARAMETERS
    train_n_samples = [32,32]
    test_n_samples = [32,32]
    beta = 1e-3
    estimator = 'Raiko'

    # RANDOMIZATION 
    seed = 1111

    # TRAINING 
    logdir = _makedir('./log/cifar10/net512/official/nll/seed1111/adagrad/lr0.1/val/')
    gen_dir = _makedir(os.path.join(logdir, 'gen'))
    max_num_epochs = 500 # 500
    optimizer_arg = {'adadelta':{'learning_rate':1, 'rho': 0.95, 'epsilon':1e-6}, #'adadelta':{'learning_rate':1, 'rho': 0.95, 'epsilon':1e-6},
                'adam':{'learning_rate':1e-4, 'beta1':0.9, 'beta2':0.999, 'epsilon':1e-8},
                'gradientdescent':{'learning_rate':0.001},
                'adagrad':{'learning_rate':0.1}}
    optimizer = 'adagrad'
    bestdir = _makedir(os.path.join(logdir, 'best'))
    display_freq = None 
    save_freq = None
    batch_size = 32 #16
    test_batch_size = 100 #16
    valid_freq = None

    # OTHERS
    turn_on_validation_in_trainset = False 

    # VIB-SPECIFICS
    embedding_dim = 256



    
