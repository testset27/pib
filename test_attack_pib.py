## test_attack.py -- sample code to test attack procedure
##
## Copyright (C) 2016, Nicholas Carlini <nicholas@carlini.com>.
##
## This program is licenced under the BSD 2-Clause licence,
## contained in the LICENCE file in this directory.

import tensorflow as tf
import numpy as np
import time
import os
import pickle
import gzip
import urllib
from config import cfg 
from model import * 

from l2_attack_pib import CarliniL2_pib

def extract_data(filename, num_images):
    with gzip.open(filename) as bytestream:
        bytestream.read(16)
        buf = bytestream.read(num_images*28*28)
        data = np.frombuffer(buf, dtype=np.uint8).astype(np.float32)
        data = (data / 255) - 0.5
        data = data.reshape(num_images, 28, 28, 1)
        return data

def extract_labels(filename, num_images):
    with gzip.open(filename) as bytestream:
        bytestream.read(8)
        buf = bytestream.read(1 * num_images)
        labels = np.frombuffer(buf, dtype=np.uint8)
    return (np.arange(10) == labels[:, None]).astype(np.float32)

class MNIST:
    def __init__(self):
        if not os.path.exists("data"):
            os.mkdir("data")
            files = ["train-images-idx3-ubyte.gz",
                     "t10k-images-idx3-ubyte.gz",
                     "train-labels-idx1-ubyte.gz",
                     "t10k-labels-idx1-ubyte.gz"]
            for name in files:

                urllib.urlretrieve('http://yann.lecun.com/exdb/mnist/' + name, "data/"+name)

        train_data = extract_data("data/train-images-idx3-ubyte.gz", 60000)
        train_labels = extract_labels("data/train-labels-idx1-ubyte.gz", 60000)
        self.test_data = extract_data("data/t10k-images-idx3-ubyte.gz", 10000)
        self.test_labels = extract_labels("data/t10k-labels-idx1-ubyte.gz", 10000)
        
        VALIDATION_SIZE = 5000
        
        self.validation_data = train_data[:VALIDATION_SIZE, :, :, :]
        self.validation_labels = train_labels[:VALIDATION_SIZE]
        self.train_data = train_data[VALIDATION_SIZE:, :, :, :]
        self.train_labels = train_labels[VALIDATION_SIZE:]


def show(img):
    """
    Show MNSIT digits in the console.
    """
    remap = "  .*#"+"#"*100
    img = (img.flatten()+.5)*3
    if len(img) != 784: return
    print("START")
    for i in range(28):
        print("".join([remap[int(round(x))] for x in img[i*28:i*28+28]]))


def generate_data(data, samples, targeted=True, start=0, inception=False, NON_RAND=False):
    """
    Generate the input data to the attack algorithm.

    data: the images to attack
    samples: number of samples to use
    targeted: if true, construct targeted attacks, otherwise untargeted attacks
    start: offset into data to use
    inception: if targeted and inception, randomly sample 100 targets intead of 1000
    """
    inputs = []
    targets = []
    labels = []

    
    if NON_RAND:
        lbs = np.argmax(data.test_labels, 1).tolist()
        sample_range = [lbs.index(k) for k in range(10)]
        start = 0
    else:
        sample_range = range(samples)

    for i in sample_range:
        if targeted:
            if inception:
                seq = random.sample(range(1,1001), 10)
            else:
                seq = range(data.test_labels.shape[1])

            for j in seq:
                if (j == np.argmax(data.test_labels[start+i])) and (inception == False):
                    continue
                inputs.append(data.test_data[start+i])
                targets.append(np.eye(data.test_labels.shape[1])[j]) # target all labels except the correct one 
                labels.append(data.test_labels[start+i])
        else:
            inputs.append(data.test_data[start+i])
            targets.append(data.test_labels[start+i])

    inputs = np.array(inputs)
    targets = np.array(targets)
    labels = np.array(labels)

    return inputs, targets, labels

def generate_zeros_to_ones(data, samples=10):
    test_labels = np.argmax(data.test_labels, 1) 
    zero_idx = np.where(test_labels == 0)[0]
    # print(zero_idx.shape)
    zero_idx = zero_idx[:samples]
    inputs = data.test_data[zero_idx]
    targets = np.eye(10)[ [1] * samples]
    labels = np.eye(10)[ [0] * samples]
    return inputs, targets, labels

def evaluate_attack(targeted = True, target_type = 'all', model= 'pib', samples = 1000, batch_size=100, NON_RAND=False, best_or_latest='latest', GPU=2, VERBOSE=False):
    """Computes the robustness of a model to a L2 targeted attack 
    Args:
        inputs: the first 1000 test MNIST images

    Returns:
        accuracy on the targeted adversarially perturbed version of the inputs (i.e., rate of successful defenses)
        rate of successful targeted attack
        the averaged magnitude of the perturbation of sucessful attacks
        the averaged magnitude of the perturbation of shifted attacks (unsucessful attack but still twists the model into a wrong label)  

    """
    sess_cfg = tf.ConfigProto(device_count = {'GPU': GPU})
    sess_cfg.gpu_options.allow_growth = True
    with tf.Session(config=sess_cfg) as sess:
        data =  MNIST()
        if best_or_latest == 'best':
            attack = CarliniL2_pib(sess, model=model, targeted=targeted, batch_size=batch_size, max_iterations=1000, confidence=0, restored_pib = cfg.bestdir )
        else:
            attack = CarliniL2_pib(sess, model=model, targeted=targeted, batch_size=batch_size, max_iterations=1000, confidence=0, restored_pib = None )

        if target_type == 'all':
            inputs, targets, labels = generate_data(data, samples=samples, targeted=targeted,
                                        start=0, inception=False, NON_RAND=NON_RAND)
        else:
            inputs, targets, labels = generate_zeros_to_ones(data, samples=samples)

        #DEBUG 
        print(inputs.shape)
        timestart = time.time()
        adv = attack.attack(inputs, targets)
        timeend = time.time()

        if VERBOSE:
            print("Took {} seconds to run {} samples".format(timeend-timestart,len(inputs)))
        n_attacks = len(adv)
        targets = np.argmax(targets, 1)
        if targeted:
            labels = np.argmax(labels, 1)
        inputs = np.reshape(inputs, (n_attacks, cfg.in_dim ))
        adv = np.reshape(adv, (n_attacks, cfg.in_dim ))

        adv_cls = []

        n_batch = n_attacks / batch_size
        for i in range(n_batch):
            o = sess.run(attack.pib.averaged_before_softmax, {attack.pib_input: adv[i* batch_size: (i+1) * batch_size]})
            o = np.argmax(o, 1)
            adv_cls.append(o)
        if n_attacks % batch_size != 0:
            o = sess.run(attack.pib.averaged_before_softmax, {attack.pib_input: adv[n_batch * batch_size: n_attacks]})
            o = np.argmax(o, 1)
            adv_cls.append(o)

        adv_cls = np.concatenate(adv_cls)
        if targeted:
            defense = np.asarray(np.equal(adv_cls, labels), np.int16)
        else:
            defense = np.asarray(np.equal(adv_cls, targets), np.int16)

        defense_rate = np.mean(defense)

        if targeted:
            success_attack = np.asarray(np.equal(adv_cls, targets), np.int16) # if target = False, this measure has no meaning
            shifted = (1 - defense) * (1- success_attack)
            success_attack_rate = np.mean(success_attack)
            shifted_rate = 1. - defense_rate - success_attack_rate
        else:
            success_attack = None
            shifted = None
            success_attack_rate = None 
            shifted_rate = None 
        
        

        l0,l1,l2,linf = compute_norm(inputs-adv)
        L = [l0, l1, l2, linf]
        if targeted:
            mags_of_success_attacks = [ np.sum(ll * success_attack) / (1. * np.sum(success_attack)) for ll in L ]
            if np.sum(shifted) == 0:
                mags_of_shifted = [0]*4
            else:
                mags_of_shifted = [ np.sum(ll * shifted) / (1. * np.sum(shifted)) for ll in L ]
        else:
            mags_of_success_attacks = None 
            mags_of_shifted = None 
        if np.sum(defense) == 0:
            mags_of_success_defense = [0,0,0,0] # no defense
        else:
            mags_of_success_defense = [ np.sum(ll * defense) / (1. * np.sum(defense)) for ll in L ]

        if targeted:
            _str = 'targeted'
        else:
            _str = 'untargeted'
        if targeted:
            np.savez(os.path.join(cfg.logdir, '%s_%s_adv_scores'%(_str, target_type)), [defense_rate] + mags_of_success_attacks)
        else:
            np.savez(os.path.join(cfg.logdir, '%s_%s_adv_scores'%(_str, target_type)), defense_rate)
        print("Defense rate = {}\nsuccessful attack rate = {}\nshifted attack = {}\nMags(attack) = {}\nMags(defense) = {}\nMags(shifted) = {}"\
            .format(defense_rate, success_attack_rate, shifted_rate, mags_of_success_attacks, mags_of_success_defense, mags_of_shifted))

        return defense, success_attack, shifted,\
                inputs, labels, adv, defense_rate, success_attack_rate, mags_of_success_attacks, mags_of_success_defense, mags_of_shifted

def compute_norm(x):
    return (
        np.linalg.norm(x, ord=0, axis=1),
        np.linalg.norm(x, ord=1, axis=1),  
        np.linalg.norm(x, ord=2, axis=1),  
        np.linalg.norm(x, ord= np.inf, axis=1))


if __name__ == "__main__":
    sess_cfg = tf.ConfigProto()
    sess_cfg.gpu_options.allow_growth = True
    with tf.Session(config=sess_cfg) as sess:
        data =  MNIST()
        attack = CarliniL2_pib(sess, batch_size=9, max_iterations=1000, confidence=0, restored_pib = cfg.logdir)
    
        inputs, targets = generate_data(data, samples=1, targeted=True,
                                        start=0, inception=False)
        timestart = time.time()
        adv = attack.attack(inputs, targets)
        timeend = time.time()
        
        print("Took",timeend-timestart,"seconds to run",len(inputs),"samples.")

        for i in range(len(adv)):
            print("Valid:")
            show(inputs[i])
            print("Adversarial:")
            show(adv[i])
            
            # print("Classification:", model.model.predict(adv[i:i+1]))
            o = sess.run(attack.pib.preds, {attack.pib_input: np.reshape(adv[i:i+1], (1, 784))})
            print("Classification: {}".format(np.argmax(o,1))) 


            print("Total distortion: {}".format(np.sum((adv[i]-inputs[i])**2)**.5))
