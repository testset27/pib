from config import cfg
from model import * 
from utils import * 
from data_load import * 
import tensorflow as tf
import json 

with open(os.path.join(cfg.logdir, 'cfg.json'), 'w') as fp:
	json.dump(vars(cfg), fp)

if cfg.non_binary:
	print('DET')
else:
	print(cfg.loss_type)
	
def one_hot_encoding(label, num_class):
	num_samples = label.shape[0]
	out = np.zeros((num_samples, num_class))
	out[np.arange(num_samples), label] = 1
	return out 


if cfg.model == 'pib':
	input = tf.placeholder(tf.float32, [None, cfg.in_dim], 'input')
	train_model = pib(input, cfg)
	# test_model = pib(input, cfg, is_train=False)

# the VIB implementation in this code base does not work properly. DO NOT use it. Use the original VIB code instead
elif cfg.model == 'vib': 
	input = tf.placeholder(tf.float32, [None, cfg.in_dim], 'input')
	train_model = vib(input, cfg)

else:
	raise ValueError('Unrecognized model!')

if cfg.dataset == 'mnist':
	from tensorflow.examples.tutorials.mnist import input_data
	mnist = input_data.read_data_sets('MNIST_data', one_hot=True)
	train_X, train_Y = mnist.train.images, mnist.train.labels 
	val_X, val_Y = mnist.validation.images, mnist.validation.labels 
	test_X, test_Y = mnist.test.images, mnist.test.labels 
	full_train_X = np.concatenate([train_X,val_X], axis=0)
	full_train_Y = np.concatenate([train_Y,val_Y], axis=0)
	trainset = DataSet(train_X, train_Y, cfg.batch_size, shuffle=True)
	devset = DataSet(val_X, val_Y, cfg.test_batch_size, shuffle=False)
	testset = DataSet(test_X, test_Y, cfg.test_batch_size, shuffle=False)
	fulltrainset = DataSet(full_train_X, full_train_Y, cfg.batch_size, shuffle=True)

	def prepare_half_mnist(X):
		upper_half = X[:, 0:392]
		lower_half = X[:, 392:]
		return upper_half, lower_half

	upper_half_full_train, lower_half_full_train = prepare_half_mnist(full_train_X)

	test_labels = np.argmax(test_Y, axis=1).tolist()
	digit_idx = []
	for i in range(10):
		digit_idx.append(test_labels.index(i))

	upper_half_test, lower_half_test = prepare_half_mnist(test_X[digit_idx])
	fulltrainset_multimodal = DataSet(upper_half_full_train, lower_half_full_train, cfg.batch_size, shuffle=True)
	testset_multimodal = DataSet(upper_half_test, lower_half_test, cfg.test_batch_size, shuffle=False)


	if cfg.output_structure in ['categorical', 'univariate_ber']:
		if cfg.VAL:
			train(train_model, cfg, trainset, devset)	
		else:
			full_train(train_model, cfg, fulltrainset, testset)
	elif cfg.output_structure == 'multivariate_ber':
		train_multimodal(train_model, test_model, cfg, fulltrainset_multimodal)
	else:
		raise ValueError('Unrecognized output structure!')

elif cfg.dataset == 'tfd':
	tfd_face_expr_X_train = np.load('./data/TFD/facial_expr_X_train.npy')
	tfd_face_expr_Y_train = np.load('./data/TFD/facial_expr_Y_train.npy')
	tfd_face_expr_X_test = np.load('./data/TFD/facial_expr_X_test.npy')
	tfd_face_expr_Y_test = np.load('./data/TFD/facial_expr_Y_test.npy')
	tfd_face_expr_X_test_unique = np.load('./data/TFD/facial_expr_X_test_unique.npy')
	tfd_trainset = DataSet(tfd_face_expr_X_train, tfd_face_expr_Y_train, cfg.batch_size, shuffle=True)

	assert cfg.output_structure in ['multivariate_ber', 'multivariate_normal']
	train_multimodal(train_model, test_model, cfg, tfd_trainset)

elif cfg.dataset == 'cifar10':
	cifar10_full_train_X = np.load('./data/cifar-10-python/cifar10_full_train_X.npy')
	cifar10_full_train_Y = np.load('./data/cifar-10-python/cifar10_full_train_Y.npy')
	cifar10_full_train_Y = one_hot_encoding( cifar10_full_train_Y, 10 )

	cifar10_test_X = np.load('./data/cifar-10-python/cifar10_test_X.npy')
	cifar10_test_Y = np.load('./data/cifar-10-python/cifar10_test_Y.npy')
	cifar10_test_Y = one_hot_encoding( cifar10_test_Y, 10 )

	cifar10_train_X = cifar10_full_train_X[:40000]
	cifar10_train_Y = cifar10_full_train_Y[:40000]

	cifar10_val_X = cifar10_full_train_X[40000:]
	cifar10_val_Y = cifar10_full_train_Y[40000:]

	trainset = DataSet(cifar10_train_X, cifar10_train_Y, cfg.batch_size, shuffle=True)
	devset = DataSet(cifar10_val_X, cifar10_val_Y, cfg.test_batch_size, shuffle=False)
	testset = DataSet(cifar10_test_X, cifar10_test_Y, cfg.test_batch_size, shuffle=False)
	fulltrainset = DataSet(cifar10_full_train_X, cifar10_full_train_Y, cfg.batch_size, shuffle=True)


	if cfg.output_structure == 'categorical':
		if cfg.VAL:
			train(train_model, cfg, trainset, devset)	
		else:
			full_train(train_model, cfg, fulltrainset, testset)
	else:
		raise ValueError('Does not support this case yet!')