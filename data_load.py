import numpy as np

class DataSet(object):
	def __init__(self, images, labels, batch_size = 128, shuffle = True):
		self.images = images
		self.labels = labels 
		self.num_examples = self.images.shape[0]
		self.data_ids = range(self.num_examples)
		self.shuffle = shuffle 
		self.batch_size = batch_size
		num_batch = self.num_examples // batch_size
		if self.num_examples % batch_size != 0:
			num_batch += 1
		self.num_batch = num_batch
		self.hard_reset()

	def get_batch(self):
		batch_size = self.batch_size
		if self.pointer + batch_size <= self.num_examples:
			ids = self.data_ids[self.pointer:self.pointer + batch_size]
			self.pointer += batch_size
		else:
			ids = self.data_ids[self.pointer:]
			self.reset()
		
		return self.images[ids], self.labels[ids]

	def get_batch_by_bstep(self, b_step):
		b_step = b_step % self.num_batch
		if b_step == 0: #shuffle when entering a new epoch 
			self.reset()
		step = b_step * self.batch_size 
		if step + self.batch_size <= self.num_examples:
			ids = self.data_ids[step:step + self.batch_size]
		else:
			ids = self.data_ids[step:]
		
		return self.images[ids], self.labels[ids]

	def reset(self):
		self.pointer = 0
		if self.shuffle:
			np.random.shuffle(self.data_ids)
	def hard_reset(self):
		np.random.seed(1234)
		self.reset()