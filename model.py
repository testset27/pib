import tensorflow as tf 
from layers import * 
from utils import * 
import math 
from data_load import * 

class pib(object):
    def __init__(self, input, config, is_train=True, scope = 'pib'):
        self.config = config
        self.in_dim = config.in_dim
        self.out_dim = config.out_dim
        self.hidden_dims = config.hidden_dims
        self.n_layers = len(self.hidden_dims)
        self.estimator = config.estimator
        self.optimizer = config.optimizer
        self.is_train = is_train 
        if is_train:
            self.n_samples = config.train_n_samples
        else:
            self.n_samples = config.test_n_samples
        # self.n_samples = config.n_samples
        # def train_n_samples(): return config.train_n_samples
        # def test_n_samples(): return config.test_n_samples 
        self.image_size = int(math.sqrt(self.in_dim)) 
        self.num_channels = 1
        self.num_labels = self.out_dim 
        self.activation = config.activation
        self.output_structure = config.output_structure 

        if config.non_binary:
            self.n_samples = [1] * self.n_layers
        self.non_binary = config.non_binary

        # self.input = tf.placeholder(tf.float32, [None,self.in_dim], name='input')
        self.input = input 
        self.target = tf.placeholder(tf.float32, [None, self.out_dim], name='target')
        tf.set_random_seed(config.seed)
        with tf.variable_scope(scope, reuse=tf.AUTO_REUSE):
            if is_train:
                self.global_step = tf.Variable(0, name='global_step', trainable=False)
            # self.is_train = tf.placeholder(tf.bool)
            # self.n_samples = tf.cond(self.is_train, train_n_samples, test_n_samples)
            if (not self.non_binary):
                self.variational_dists = get_factorized_ber(self.hidden_dims)
            if self.config.output_structure == 'multivariate_normal':
                self.sigma = tf.get_variable('sigma', initializer= np.sqrt(np.var(tfd_face_expr_Y_train, axis=0)).astype('float32'))  #tf.truncated_normal_initializer())
            self.build_model()
            self.estimate_vcr()
            self.inference()
        if is_train:
            self.build_train()
            self.init_op = tf.global_variables_initializer()
        self.summary()
        count_params(scope)

    def build_model(self):
        dims = [self.in_dim] + self.hidden_dims + [self.out_dim]
        mi = 0
        layers = {0:self.input}
        for i in range(1, self.n_layers + 2):
            with tf.variable_scope('layer_%d'%(i)): # REDUNDANT!
                try: 
                    if self.config.reload:
                        print('Load pretrained weights for layer %d'%(i))
                        pretrained_w = np.load(os.path.join(self.config.load_pretrained_from, 'w.npy'))[i-1].astype('float32')
                        pretrained_b = np.load(os.path.join(self.config.load_pretrained_from, 'b.npy'))[i-1].astype('float32')
                        pre_activate = revised_ff(layers[i-1], dims[i-1:i+1], \
                                                kernel_initializer=pretrained_w, 
                                                bias_initializer=pretrained_b,
                                                reload=True, trainable=False, scope = 'layer_%d'%(i))

                    else:
                        pre_activate = revised_ff(layers[i-1], dims[i-1:i+1] , scope = 'layer_%d'%(i))
                except:
                    pre_activate = revised_ff(layers[i-1], dims[i-1:i+1] , scope = 'layer_%d'%(i))
                if i < self.n_layers + 1:
                    if self.non_binary:
                        if self.activation == 'sigmoid':
                            layers[i] = tf.sigmoid(pre_activate)
                        elif self.activation == 'relu':
                            layers[i] = tf.relu(pre_activate)
                        else:
                            raise ValueError('Unrecognized activation')
                    else:
                        o, _mi = bsn_wrapper(pre_activate, self.variational_dists[i-1], self.n_samples[i-1], self.estimator)
                        layers[i] = o
                        mi += _mi 
                else: # prediction layer
                    layers[i] = pre_activate
        self.layers = layers
        self.output = layers[self.n_layers+1]
        self.mi = mi
    
    def estimate_vcr(self):
        if self.non_binary:
            if self.config.output_structure == 'categorical':
                self.nll = -tf.reduce_mean(tf.reduce_sum(self.target * tf.log(tf.nn.softmax(self.output) + 1e-8), 1))
            elif self.config.output_structure == 'multivariate_ber':
                self.nll = -tf.reduce_mean(
                    tf.reduce_sum(
                        self.target * tf.log(tf.sigmoid(self.output) + 1e-8) + 
                        (1. - self.target) * tf.log(1. - tf.sigmoid(self.output) + 1e-8) , 
                        axis=1)
                    )
            else:
                assert self.config.output_structure == 'univariate_ber'
                self.nll = -tf.reduce_mean(
                        self.target * tf.log(tf.sigmoid(self.output) + 1e-8) + 
                        (1. - self.target) * tf.log(1. - tf.sigmoid(self.output) + 1e-8)
                    )
            self.vcr = self.nll 
            self.pib = self.nll 
        else:
            vcr = []
            if self.config.output_structure == 'categorical':
                p = tf.nn.softmax(self.output)
                for i in range(self.n_layers + 1):
                    # p_rshp = tf.reshape(p, (-1, self.n_samples ** i, self.n_samples ** (self.n_layers - i), \
                    #                         self.out_dim))
                    p_rshp = tf.reshape(p, (-1, int(np.prod(self.n_samples[:i])), int(np.prod(self.n_samples[i:])), \
                                            self.out_dim))
                    vcr.append( 
                        -tf.reduce_mean(
                            tf.reduce_sum( # sum along the output dimension
                                self.target * tf.reduce_mean(tf.log(tf.reduce_mean(p_rshp, 2) + 1e-8), 1), -1
                            )
                        )
                    )
            elif self.config.output_structure == 'univariate_ber': # binary classification
                p = tf.sigmoid(self.output)
                for i in range(self.n_layers + 1):
                    p_rshp = tf.reshape(p, (-1, int(np.prod(self.n_samples[:i])), int(np.prod(self.n_samples[i:])), \
                                            self.out_dim))
                    vcr.append( 
                        -tf.reduce_mean(            
                                self.target * tf.reduce_mean(tf.log(tf.reduce_mean(p_rshp, 2) + 1e-8), 1) + \
                                (1. - self.target) * tf.reduce_mean(tf.log(tf.reduce_mean(1. - p_rshp, 2) + 1e-8), 1)
                        )
                    )
            elif self.config.output_structure == 'multivariate_normal':
                mu = tf.sigmoid(self.output) 
                # mu = self.output 
                ## This version implements Equation (4) in my paper which is not stable for the case of Gaussian 
                # sigma_rshp = tf.expand_dims(tf.expand_dims(tf.expand_dims(self.sigma, 0), 0), 0) # [1,1,1,d]
                # for i in range(self.n_layers + 1):
                #     mu_rshp = tf.reshape(mu, (-1, self.n_samples ** i, self.n_samples ** (self.n_layers - i), \
                #                             self.out_dim)) 
                #     y_rshp = tf.expand_dims(tf.expand_dims(self.target, 1), 1) # [b, 1, 1, d]
                #     vcr.append(
                #         -tf.reduce_mean( # mean over dim 0 and dim 1
                #             tf.log(
                #                 tf.reduce_mean(
                #                     tf.exp( 
                #                         tf.reduce_sum(-0.5 * ((y_rshp - mu_rshp) ** 2) / (sigma_rshp ** 2) - 0.5 * tf.log( sigma_rshp ** 2), axis=-1) 
                #                         # ignored constant - 0.5 * d * log (2 * pi) 
                #                     ),
                #                     axis=2)
                                    
                #             )
                #         )
                #     )

                ## This version is an upper bound on the Equation (4) which turns out to not depend on l 
                mu_rshp = tf.reshape(mu, (-1, int(np.prod(self.n_samples)), self.out_dim))
                sigma_rshp = tf.expand_dims(tf.expand_dims(self.sigma, 0), 0) # [1,1,d]
                y_rshp = tf.expand_dims(self.target, 1) # [b, 1, d]
                
                vcr_ = -tf.reduce_mean(
                    tf.reduce_sum(-0.5 * ((y_rshp - mu_rshp) ** 2) / (sigma_rshp ** 2) - 0.5 * tf.log( sigma_rshp ** 2), axis=-1)
                )

                vcr = [vcr_] * (self.n_layers + 1)
                # # This is multivariate Ber 
                # p = tf.sigmoid(self.output)
                # for i in range(self.n_layers + 1):
                #     p_rshp = tf.reshape(p, (-1, self.n_samples ** i, self.n_samples ** (self.n_layers - i), \
                #                             self.out_dim)) 
                #     vcr.append( 
                #         -tf.reduce_mean(
                #             tf.reduce_sum( # sum over the ouput dimension since VCR for multivarate Bernoulli variables decomposes into the sum of VCR of its components           
                #                 self.target * tf.reduce_mean(tf.log(tf.reduce_mean(p_rshp, 2) + 1e-8), 1) + \
                #                 (1. - self.target) * tf.reduce_mean(tf.log(tf.reduce_mean(1. - p_rshp, 2) + 1e-8), 1),
                #             axis=-1)
                #         )
                #     )
            else:
                assert self.config.output_structure == 'multivariate_ber'
                p = tf.sigmoid(self.output)
                for i in range(self.n_layers + 1):
                    p_rshp = tf.reshape(p, (-1, int(np.prod(self.n_samples[:i])), int(np.prod(self.n_samples[i:])), \
                                            self.out_dim)) 
                    vcr.append( 
                        -tf.reduce_mean(
                            tf.reduce_sum( # sum over the ouput dimension since VCR for multivarate Bernoulli variables decomposes into the sum of VCR of its components           
                                self.target * tf.reduce_mean(tf.log(tf.reduce_mean(p_rshp, 2) + 1e-8), 1) + \
                                (1. - self.target) * tf.reduce_mean(tf.log(tf.reduce_mean(1. - p_rshp, 2) + 1e-8), 1),
                            axis=-1)
                        )
                    )
        
            self.nll = vcr[0]
            self.vcr = tf.add_n(vcr)
            self.pib = self.vcr + self.config.beta * self.mi
        if self.config.loss_type == 'nll':
            self.loss = self.nll 
        if self.config.loss_type == 'vcr':
            self.loss = self.vcr 
        if self.config.loss_type == 'pib':
            self.loss = self.pib 

    def build_train(self):
        self.optimizer = optimizer_factory[self.optimizer](**self.config.optimizer_arg[self.optimizer])
        self.train_op = self.optimizer.minimize(self.loss, global_step=self.global_step)
  
    def inference(self):
        if self.config.output_structure == 'categorical':
            p = tf.nn.softmax(self.output)
            gts = tf.argmax(self.target,1)
            self.preds = tf.reduce_mean(
                tf.reshape(p, (-1, int(np.prod(self.n_samples)), self.out_dim)), 1
            )
            preds = tf.argmax(self.preds, 1)
        elif self.config.output_structure == 'univariate_ber':
            p = tf.sigmoid(self.output)
            gts = self.target
            self.preds = tf.reduce_mean(
                tf.reshape(p, (-1, int(np.prod(self.n_samples)), self.out_dim)), 1
            )
            preds = tf.cast(self.preds > 0.5, tf.float32) 
        else:
            raise ValueError('Not implemented for %s yet!'%(self.config.output_structure))

        
        _count = tf.cast(tf.equal(preds, gts), tf.float32)
        self.err = (1. -  tf.reduce_mean(_count)) * 100.
        # average before-softmax: seems weird but let's see if it works. This is used for L2 adversarial attack 
        self.averaged_before_softmax = tf.reduce_mean(
                tf.reshape(self.output, (-1, int(np.prod(self.n_samples)), self.out_dim)), 1
            )

    def summary(self):
        self.valid_err =  tf.Variable(tf.constant(0.0, shape=(), dtype = tf.float32),trainable=False, name="valid_err")
        self.valid_err_placeholder = tf.placeholder(tf.float32, shape = (), name = "valid_err_placeholder")
        self.valid_err_assign = tf.assign(self.valid_err, self.valid_err_placeholder)

        train_loss_summ = tf.summary.scalar('train/loss', self.loss)
        self.train_loss_summ = tf.summary.merge([train_loss_summ])

        valid_err_summ = tf.summary.scalar('err', self.valid_err)
        self.valid_err_summ = tf.summary.merge([valid_err_summ])

        if (not self.non_binary) and (self.config.loss_type != 'nll'):
            r_tensor_summ = []
            for i in range(self.n_layers):
                _mean = tf.reduce_mean(self.variational_dists[i])
                _stddev = tf.sqrt(tf.reduce_mean(tf.square(self.variational_dists[i] - _mean)))
                _min = tf.reduce_min(self.variational_dists[i])
                _max = tf.reduce_max(self.variational_dists[i])
                r_tensor_summ.append(tf.summary.scalar('r/layer_%d/mean'%(i),  _mean ))
                r_tensor_summ.append(tf.summary.scalar('r/layer_%d/stddev'%(i),  _stddev ))
                r_tensor_summ.append(tf.summary.scalar('r/layer_%d/mean'%(i),  _mean ))
                r_tensor_summ.append(tf.summary.scalar('r/layer_%d/min'%(i),  _min ))
                r_tensor_summ.append(tf.summary.scalar('r/layer_%d/max'%(i),  _max ))
                # r_tensor_summ.append(tf.summary.histogram('r/layer_%d/histogram'%(i),  self.variational_dists[i] ))
                self.r_tensor_summ = tf.summary.merge(r_tensor_summ)

    def exact_mi(self, sess, train_X, train_Y, save_file = None):
        w = []
        b = []
        for l in range(self.n_layers + 1):
            w_ = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='pib/layer_%d/layer_%d/w:0'%(l+1, l+1))[0]
            w_ = sess.run(w_)
            w.append(w_)
            b_ = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='pib/layer_%d/layer_%d/b:0'%(l+1, l+1))[0]
            b_ = sess.run(b_)
            b.append(b_)
        return compute_exact_mi(train_X, train_Y, w, b, self.n_layers, self.hidden_dims,  save_file)

    def save(self, sess):
        w = []
        b = []
        for l in range(self.n_layers + 1):
            w_ = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='pib/layer_%d/layer_%d/w:0'%(l+1, l+1))[0]
            w_ = sess.run(w_)
            w.append(w_)
            b_ = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='pib/layer_%d/layer_%d/b:0'%(l+1, l+1))[0]
            b_ = sess.run(b_)
            b.append(b_)
            np.save(os.path.join(self.config.save_trained_to, 'w.npy'), w)
            np.save(os.path.join(self.config.save_trained_to, 'b.npy'), b)
# class vib(object):
#     def __init__(self, input, config, scope = 'vib'):
#         self.config = config
#         self.in_dim = config.in_dim
#         self.out_dim = config.out_dim
#         self.hidden_dims = config.hidden_dims
#         self.n_layers = len(self.hidden_dims)
#         self.optimizer = config.optimizer
#         self.image_size = int(math.sqrt(self.in_dim)) 
#         self.num_channels = 1
#         self.num_labels = self.out_dim 
#         self.activation = config.activation
#         self.embedding_dim = config.embedding_dim
#         self.input = input 
#         self.target = tf.placeholder(tf.float32, [None, self.out_dim], name='target')
#         tf.set_random_seed(config.seed)
#         with tf.variable_scope(scope, reuse=tf.AUTO_REUSE):
#             self.global_step = tf.Variable(0, name='global_step', trainable=False)
#             self.encoder()
#             self.loss()
#             self.prediction()
#         self.build_train()
#         self.summary()
#         self.init_op = tf.global_variables_initializer()
#         count_params(scope)
#     def encoder(self):
#         dims = [self.in_dim] + self.hidden_dims + [self.out_dim]
#         layers = {0:self.input}
#         for i in range(1, self.n_layers + 1):
#             with tf.variable_scope('layer_%d'%(i)): # REDUNDANT!
#                 pre_activate = ff(layers[i-1], dims[i-1:i+1], scope = 'layer_%d'%(i))
#                 if self.activation == 'sigmoid':
#                     layers[i] = tf.sigmoid(pre_activate)
#                 elif self.activation == 'relu':
#                     layers[i] = tf.relu(pre_activate)
#                 else:
#                     raise ValueError('Unrecognized activation')
#         layers['mu'] = ff(layers[self.n_layers], [dims[-2], self.embedding_dim], scope='mu_layer')
#         layers['sigma'] = 1e-8 + tf.nn.softplus(ff(layers[self.n_layers], [dims[-2], self.embedding_dim], scope='sigma_layer')) # > 0
#         self.layers = layers
#         self.dist = tf.contrib.distributions.Normal(tf.zeros(self.embedding_dim), tf.ones(self.embedding_dim))
#         epsilon = self.dist.sample([1])
#         self.z = layers['mu'] +  epsilon * layers['sigma']

#     def decoder(self, z):
#         return tf.nn.softmax(ff(z, [self.embedding_dim, self.out_dim], scope='output_layer'), -1)

#     def prediction(self):
#         samples = self.dist.sample([self.config.n_samples])
#         samples = tf.expand_dims(samples,0)
#         z = tf.expand_dims(self.layers['mu'],1) + samples * tf.expand_dims(self.layers['sigma'],1)
#         o = self.decoder(z)
#         o = tf.reduce_sum(o, 1)
#         gts = tf.argmax(self.target,1)
#         preds = tf.argmax(o, 1)
#         _count = tf.cast(tf.equal(preds, gts), tf.float32)
#         self.err = (1. -  tf.reduce_mean(_count)) * 100.

#     def loss(self):
#         self.output = self.decoder(self.z)
#         marginal_likelihood = tf.reduce_sum(self.target * tf.log(self.output + 1e-8) + (1 - self.target) * tf.log(1 - self.output + 1e-8), 1)
#         KL_divergence = 0.5 * tf.reduce_sum(tf.square(self.layers['mu']) + tf.square(self.layers['sigma']) - tf.log(1e-8 + tf.square(self.layers['sigma'])) - 1, 1)
#         marginal_likelihood = tf.reduce_mean(marginal_likelihood)
#         KL_divergence = tf.reduce_mean(KL_divergence)
#         ELBO = marginal_likelihood - self.config.beta * KL_divergence
#         self.loss = -ELBO

#     def build_train(self):
#         self.optimizer = optimizer_factory[self.optimizer](**self.config.optimizer_arg[self.optimizer])
#         self.train_op = self.optimizer.minimize(self.loss, global_step=self.global_step)

#     def summary(self):
#         self.valid_err =  tf.Variable(tf.constant(0.0, shape=(), dtype = tf.float32),trainable=False, name="valid_err")
#         self.valid_err_placeholder = tf.placeholder(tf.float32, shape = (), name = "valid_err_placeholder")
#         self.valid_err_assign = tf.assign(self.valid_err, self.valid_err_placeholder)

#         train_loss_summ = tf.summary.scalar('train/loss', self.loss)
#         self.train_loss_summ = tf.summary.merge([train_loss_summ])

#         valid_err_summ = tf.summary.scalar('err', self.valid_err)
#         self.valid_err_summ = tf.summary.merge([valid_err_summ])