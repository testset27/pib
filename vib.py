import tensorflow as tf 
import numpy as np
import math

layers  =  tf.contrib.layers
ds = tf.contrib.distributions
prior = ds.Normal(0.0, 1.0)

class vib(object):
    def __init__(self, images):
        self.labels = tf.placeholder(tf.int64, [None], 'labels')
        one_hot_labels = tf.one_hot(self.labels, 10)

        with tf.variable_scope('encoder'):
            encoding = encoder(images)
        with tf.variable_scope('decoder'):
            self.logits = decoder(encoding.sample())
        with tf.variable_scope('decoder', reuse=True):
            self.many_logits = decoder(encoding.sample(12))
        self.averaged_before_softmax = self.logits 

def encoder(images):
    net = tf.nn.sigmoid(layers.linear(2*images-1, 512))
    net = tf.nn.sigmoid(layers.linear(net, 512))
    params = layers.linear(net, 512)
    mu, rho = params[:, :256], params[:, 256:]
    encoding = ds.NormalWithSoftplusScale(mu, rho - 5.0)
    return encoding

def decoder(encoding_sample):
    net = layers.linear(encoding_sample, 10)
    return net