import tensorflow as tf
import numpy as np 
from utils import generate_full_binary_modes, np_sigmoid

def ff(inputs, shape,
        kernel_initializer = tf.contrib.layers.xavier_initializer,
        bias_initializer =  tf.zeros_initializer, 
        trainable=True,
        scope='ff', reuse=None):
    with tf.variable_scope(scope, reuse=tf.AUTO_REUSE):
        w = tf.get_variable('w', shape, initializer=kernel_initializer(), trainable=trainable)
        b = tf.get_variable('b', shape[-1:], initializer=bias_initializer(), trainable=trainable)
        inp_shape = inputs.shape.as_list()
        if len(inp_shape) > 2:
            output = tf.reshape(
                tf.matmul(tf.reshape(inputs, (-1, inp_shape[-1])), w) + b, \
                [-1] + inp_shape[1:-1] + [shape[-1]]) 
        else:
            output = tf.matmul(inputs, w) + b
        return output

def revised_ff(inputs, shape,
        kernel_initializer = tf.contrib.layers.xavier_initializer,
        bias_initializer =  tf.zeros_initializer, 
        trainable=True,
        reload = False, 
        scope='ff', reuse=None):
    with tf.variable_scope(scope, reuse=tf.AUTO_REUSE):
        if not reload:
            w = tf.get_variable('w', shape, initializer=kernel_initializer(), trainable=trainable)
            b = tf.get_variable('b', shape[-1:], initializer=bias_initializer(), trainable=trainable)
        else:
            w = tf.get_variable('w', initializer= kernel_initializer.astype('float32'), trainable=trainable)
            b = tf.get_variable('b', initializer= bias_initializer.astype('float32'), trainable=trainable)
        inp_shape = inputs.shape.as_list()
        if len(inp_shape) > 2:
            output = tf.reshape(
                tf.matmul(tf.reshape(inputs, (-1, inp_shape[-1])), w) + b, \
                [-1] + inp_shape[1:-1] + [shape[-1]]) 
        else:
            output = tf.matmul(inputs, w) + b
        return output

def get_factorized_ber(hidden_dims, initializer = tf.truncated_normal_initializer ,scope='factorizd_ber'):
    with tf.variable_scope(scope, reuse=tf.AUTO_REUSE):
        r = []
        for i,dim in enumerate(hidden_dims):
            r.append(tf.sigmoid(tf.get_variable('r_%d'%(i+1), (dim), initializer=initializer())))
        return r 

def bsn_Raiko(x, r, n_samples, slope_tensor = None, scope='bsn_Raiko'):
    """
    Args:
        x: [batch, dim]

    Returns:
        o: [batch, n, dim]
    """
    with tf.variable_scope(scope):
        if slope_tensor is None:
            slope_tensor = tf.constant(1.0)
        p = tf.sigmoid(slope_tensor * x)
        shp = x.get_shape().as_list()
        rank = len(shp)
        # def fn(x):
        #     y = tf.ceil(p - tf.random_uniform(tf.shape(p)))
        #     return p + tf.stop_gradient(y - p)
        
        # outputs = tf.map_fn(fn, tf.range(n_samples), tf.float32)
        # perm = range(1, len(outputs.get_shape().as_list()) + 1)
        # perm[-1] = perm[-1] - 1 
        # perm[-2] = 0 
        # o = tf.transpose(outputs, perm)
        outputs = []
        for _ in range(n_samples):
            y = tf.ceil(p - tf.random_uniform(tf.shape(p)))
            z = p + tf.stop_gradient(y - p)
            z = tf.expand_dims(z, rank-1) #[batch, 1, dim]
            outputs.append(z)
        o = tf.concat(outputs, rank-1)
        mi = estimate_mi(p, r)
        return o, mi 

def bsn_ST(x, r, n_samples, slope_tensor = None, scope='bsn_ST'):
    with tf.variable_scope(scope):
        if slope_tensor is None:
            slope_tensor = tf.constant(1.0)
        outputs = []
        p = tf.sigmoid(slope_tensor * x)
        shp = x.get_shape().as_list()
        rank = len(shp)
        for _ in range(n_samples):
            y = tf.ceil(p - tf.random_uniform(tf.shape(p)))
            z = x + tf.stop_gradient(y - x)
            z = tf.expand_dims(z, rank-1)
            outputs.append(z)
        o = tf.concat(outputs, rank-1)
        mi = estimate_mi(p, r)
        return o, mi 

def bsn_REINFORCE(x, r, n_samples, slope_tensor = None, scope='bsn_REINFORCE'): #? 
    with tf.variable_scope(scope):
        if slope_tensor is None:
            slope_tensor = tf.constant(1.0)
        outputs = []
        p = tf.sigmoid(slope_tensor * x)
        shp = x.get_shape().as_list()
        rank = len(shp)
        for _ in range(n_samples):
            y = tf.ceil(p - tf.random_uniform(tf.shape(p)))
            t = x * tf.stop_gradient(p * (1  - p))
            z = t + tf.stop_gradient(y - t)
            z = tf.expand_dims(z, rank-1)
            outputs.append(z)
        o = tf.concat(outputs, rank-1)
        mi = estimate_mi(p, o)
        return o, mi 

def RELU_bsn(x, r, n_samples, shift = 0.5, scope='RELU_bsn'):
    """Convert a relu-activated neuron into a binary stochastic one. 
    A variant of ST gradient estimator is used.

    *On development 
    """
    with tf.variable_scope(scope):
        _a = tf.nn.relu(x)
        p = tf.minimum(_a, tf.sigmoid(x - shift))
        shp = x.get_shape().as_list()
        rank = len(shp)
        outputs = []
        for _ in range(n_samples):
            y = tf.ceil(p - tf.random_uniform(tf.shape(p)))
            z = _a + tf.stop_gradient(y - _a) # a variant of ST 
            z = tf.expand_dims(z, rank-1)
            outputs.append(z)
        o = tf.concat(outputs, rank-1)
        mi = estimate_mi(p, o)
        return o, mi 

def estimate_mi(p, r):
    """
    Args:
        p: [b, dim]
        r: [dim]
    """
    dim = p.get_shape().as_list()[-1]
    p = tf.reshape(p, (-1, dim))
    r = tf.expand_dims(r, 0)
    mi = tf.reduce_mean(tf.reduce_sum(p * (tf.log( p + 1e-16) - tf.log(r + 1e-16)) + (1. - p) * (tf.log( 1. - p + 1e-16) - tf.log(1. - r + 1e-16)), -1))
    return mi 


def bsn_wrapper(pre_activate_tensor, r,
                n_samples,
                estimator = 'Raiko'):
    if estimator == 'Raiko':
        return bsn_Raiko(pre_activate_tensor, r, n_samples)
    elif estimator == 'ST':
        return bsn_ST(pre_activate_tensor, r, n_samples)
    elif estimator == 'REINFORCE':
        return bsn_REINFORCE(pre_activate_tensor, r,  n_samples)
    elif estimator == 'RELU_Raiko':
        return RELU_bsn(pre_activate_tensor, r, n_samples)
    else:
        raise ValueError('Unrecognized estimator.')

POSITIVE_ENOUGH_BUT_VERY_CLOSE_TO_ZERO = 1e-45 #FixeD!
def stable_log(x):
    """
    ARG:
        x >= 0 
    """
    return np.log( np.clip(x, POSITIVE_ENOUGH_BUT_VERY_CLOSE_TO_ZERO, None) )


def compute_exact_mi(train_X, train_Y, w, b, n_layers, hidden_dims,  save_file = None):
    """Exhaustedly computes I(Zi, X) and I(Zi, Y) over the given empirical estimate p(X,Y) only for Bernoulli-IBNN
    """
    # compute in CPU 
    _C = 2 
    # train_Y_bin = np.argmax(train_Y, axis= 1).astype(np.int32) % 2

    # pxy = T.stack([1.* train_Y_bin, 1. - train_Y_bin], axis=1)[:,:,0]
    pxy_odd = 1.* train_Y / train_Y.shape[0]
    pxy_even = (1. - train_Y) / train_Y.shape[0]
    pxy = np.squeeze(np.stack([pxy_odd, pxy_even], axis=1), axis=2) 

    #DEBUG
    # print('DEBUG MODE')
    # print(pxy_odd.shape, pxy_even.shape, pxy.shape)

    # Symbolic computation
    n_ex = train_X.shape[0]
    ib_var = [train_X]
    p_var = [  np.asarray([1. /  n_ex] * n_ex)   ]
    pz_given_prevz = []
    mi_zx = []
    mi_zy = []
    p_zl_given_x = []

    # get variables by name

    # ib_var[l]: (n_sample, dim)
    # p_var[l]: (n_sample)
    # pz_given_prevz: (nz, nprevz)
    for l in range(n_layers):
        # p(zl | zprevl)
        full_modes = generate_full_binary_modes(hidden_dims[l])# (2^nl, nl)
        prev_var = ib_var[l]
        # w = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='pib/layer_%d/layer_%d/w:0'%(l+1, l+1))[0]
        # w = sess.run(w)
        # b = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='pib/layer_%d/layer_%d/b:0'%(l+1, l+1))[0]
        # b = sess.run(b)
        pre_act = np.matmul(prev_var, w[l]) + b[l]
        pre_act_reshp = np.reshape(pre_act, (1, -1, hidden_dims[l])) # (1, n_sample, nl)
        full_modes_reshp = np.reshape(full_modes, (-1, 1, hidden_dims[l])) # (2^nl, 1, nl)
        pz_given_prevz_t = np.exp(np.sum(  stable_log( np_sigmoid( (2 *full_modes_reshp - 1) * pre_act_reshp )   ), axis = -1))

        pz = np.sum( pz_given_prevz_t * np.reshape( p_var[l], (1,-1)), axis=1) # (2^nl)
        _ent_z = pz * stable_log(pz)
        ent_z = - np.sum(np.where( np.isnan(_ent_z), np.zeros_like(_ent_z), _ent_z ) )

        # update p_var, ib_var 
        ib_var += [full_modes]
        # ib_var[l] = None # free memory 
        p_var += [pz]
        # p_var[l] = None 
        pz_given_prevz += [pz_given_prevz_t]
        # compute p(z|x) 
        count = 0
        for elem in pz_given_prevz.__reversed__():
            if count == 0:
                pz_given_x_t = elem
            else:
                pz_given_x_t = np.matmul(pz_given_x_t, elem)
            count += 1

        p_zl_given_x  += [pz_given_x_t]

        _ent_pz_given_x = pz_given_x_t * stable_log(pz_given_x_t)

        ent_pz_given_x = np.where( np.isnan(_ent_pz_given_x), np.zeros_like(_ent_pz_given_x), _ent_pz_given_x )

        mi_zx +=  [ent_z +  np.sum(np.mean(ent_pz_given_x, axis=-1))   ] # mutiply by p(x) = 1/n -> mean(., axis=-1)
        pzy = np.transpose(np.matmul(pz_given_x_t, pxy))
        mi_zy += [np.log(_C) - np.sum(np.sum(-pzy * (stable_log(pzy) - stable_log(pz)) ),axis=0) ] #Correct!

    ## compute p(y|z_l)
    # compute p(z_L | z_l)
    mi_zy_variational = []
    # w = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='pib/layer_%d/layer_%d/w:0'%(n_layers+1, n_layers+1))[0]
    # w = sess.run(w)
    # b = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='pib/layer_%d/layer_%d/b:0'%(n_layers+1, n_layers+1))[0]
    # b = sess.run(b)
    p_y_given_zL = np_sigmoid(np.matmul(ib_var[-1], w[-1]) + b[-1]) # change softmax to sigmoid for decision problem, sigmoid predicts prob of being odd
    for l in range(n_layers, -1, -1):
        if l == n_layers:
            zL_n_fullmodes = ib_var[-1].shape[0]
            p_zL_given_zl = np.eye(zL_n_fullmodes) #T.identity_like( T.zeros( (zL_n_fullmodes,zL_n_fullmodes) ))
        else:
            p_zL_given_zl = np.matmul(p_zL_given_zl, pz_given_prevz[l])

        p_y_given_zl = np.matmul(np.transpose(p_y_given_zL), p_zL_given_zl)[0]
        if l > 0:
            tmp_odd = np.matmul(np.transpose(p_zl_given_x[l-1]), np.transpose(stable_log(p_y_given_zl)))
            tmp_even = np.matmul( np.transpose(p_zl_given_x[l-1]), np.transpose(stable_log(1. - p_y_given_zl)))

            mi_zy_variational += [np.log(_C) + np.sum(pxy_odd * tmp_odd + pxy_even * tmp_even)]

        else:
            mi_zy_variational += [np.log(_C) + np.sum(pxy_odd *  stable_log(p_y_given_zl) + pxy_even * stable_log(1. - p_y_given_zl) )]
    monitor = np.stack(mi_zx + mi_zy + mi_zy_variational) # c1, c2, c3, r1, r2, r3, v3, v2, v1, v0
    # DETECT NAN 
    if len(np.argwhere(np.isnan(mi_zy))) > 0:
        raise Exception('NaN in I(Z,Y)!')
    # monitor = sess.run(monitor)
    if save_file is not None:
        np.save(save_file, monitor)

    return monitor