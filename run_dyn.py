from config import cfg
from model import * 
from utils import * 
from utils import _makedir
from data_load import * 
import tensorflow as tf 

cfg.hidden_dims = [10,10,10]
cfg.out_dim = 1
cfg.loss_type = 'pib'
cfg.beta = 1e-4
cfg.batch_size = 32
cfg.max_num_epochs = 200
cfg.seed = 4444 #1234, 1111, 2222, 3333, 4444
cfg.logdir = _makedir('./log/dyn/pib/beta4/%d'%(cfg.seed))

print(cfg.logdir)

trainset = DataSet(train_X, train_Y, cfg.batch_size, shuffle=True)
input = tf.placeholder(tf.float32, [None, cfg.in_dim], 'input')
model = pib(input, cfg)

batch_size = trainset.batch_size
n_batch = trainset.num_batch
max_iters = n_batch * cfg.max_num_epochs
valid_freq = cfg.valid_freq
if valid_freq is None:
    valid_freq = n_batch
train_op = model.train_op


sess_cfg = tf.ConfigProto(device_count = {'GPU': 2});
sess_cfg.gpu_options.allow_growth = True
saver = tf.train.Saver()
# sv = tf.train.Supervisor(logdir=cfg.logdir, 
#                         global_step = model.global_step, 
#                         summary_op=None,
#                         init_op=model.init_op)
sess = tf.InteractiveSession(config=sess_cfg)
sess.run(model.init_op)

# with sv.managed_session(config = sess_cfg) as sess:
save_file = os.path.join(cfg.logdir, 'mi_0')
monitor = model.exact_mi(sess,train_X, train_Y, save_file )
last_step = sess.run(model.global_step)
print('last_step = {}'.format(last_step))
ep = last_step // n_batch
for _ in range(ep):
    trainset.reset()
while ep <= cfg.max_num_epochs:
    sys.stdout.write("\rProgress {}/{} ...   ".format(last_step, max_iters))
    sys.stdout.flush()
#     if sv.should_stop():
#         break 
    last_step = sess.run(model.global_step)
    x,y = trainset.get_batch_by_bstep(last_step)
    y = np.argmax(y,1).astype(np.int32) % 2
    y = np.expand_dims(y.astype(np.float32), 1)
    feed_dict = {model.input:x, model.target:y}
    _, tmp_loss, step = sess.run([train_op, model.loss, model.global_step], feed_dict)
    assert step == last_step + 1

    ep = (step - 1) // n_batch

    if step % cfg.display_freq == 0:
        st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        print("\n[{}]Epoch = {}, Step = {}, Loss = {}".format(st, ep, step, tmp_loss))
    if step % n_batch == 0:
        save_file = os.path.join(cfg.logdir, 'mi_%d'%(ep+1))
        monitor = model.exact_mi(sess,train_X, train_Y, save_file )