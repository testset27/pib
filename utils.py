import numpy as np 
import tensorflow as tf
import datetime, time, os, shutil, sys, json, pickle
try:
    import PIL.Image as Image
except ImportError:
    import Image

from data_load import * 

optimizer_factory = {"adadelta":tf.train.AdadeltaOptimizer,
            "adam":tf.train.AdamOptimizer,
            "gradientdescent":tf.train.GradientDescentOptimizer,
            "adagrad":tf.train.AdagradOptimizer}

def count_params(scope_prefix):
    total_parameters = 0
    var_list = tf.trainable_variables()
    var_names = [v.name for v in var_list]
    print('Trainable variables: ')
    for v in var_names:
        print(v)
    effective_var_list = [v for v in var_list if scope_prefix in v.name]
    for variable in effective_var_list:
        shape = variable.get_shape()
        variable_parametes = 1
        for dim in shape:
            variable_parametes *= dim.value
        total_parameters += variable_parametes
    print("The total number of trainable parameters: {}".format(total_parameters))

def _makedir(path):
    if not os.path.isdir(path):
        os.makedirs(path)
    return path

def np_sigmoid(x):
    return 1 / ( 1 + np.exp(-x))

def matmul(a, b):
    a_shape = a.shape.as_list()
    if len(a_shape) == 1:
        a = tf.expand_dims(a,0)
    b_shape = b.shape.as_list()
    if len(b_shape) == 1:
        b = tf.expand_dims(b,1)
    if len(a_shape) > 2:
        output = tf.reshape(
            tf.matmul(tf.reshape(a, (-1, a_shape[-1])), b), \
            [-1] + a_shape[1:-1] + [b_shape[-1]]) 
    else:
        output = tf.matmul(a, b)
    return output    

def generate_full_binary_modes(n):
    """Returns a matrix of size 2^n * n containings all 2^n possible binary modes of size n   
    """
    a = np.zeros(shape=(2**n, n), dtype='float32')
    for i in range(2**n):
        l = list('{0:0b}'.format(i))
        a[i,:] = np.asarray([0]* (n - len(l)) + [int(x) for x in l])
    return a

def scale_to_unit_interval(ndar, eps=1e-8):
    """ Scales all values in the ndarray ndar to be between 0 and 1 """
    ndar = ndar.copy()
    ndar -= ndar.min()
    ndar *= 1.0 / (ndar.max() + eps)
    return ndar

def tile_raster_images(X, img_shape, tile_shape, tile_spacing=(0, 0),
                       scale_rows_to_unit_interval=True,
                       output_pixel_vals=True):
    """
    Transform an array with one flattened image per row, into an array in
    which images are reshaped and layed out like tiles on a floor.

    This function is useful for visualizing datasets whose rows are images,
    and also columns of matrices for transforming those rows
    (such as the first layer of a neural net).

    :type X: a 2-D ndarray or a tuple of 4 channels, elements of which can
    be 2-D ndarrays or None;
    :param X: a 2-D array in which every row is a flattened image.

    :type img_shape: tuple; (height, width)
    :param img_shape: the original shape of each image

    :type tile_shape: tuple; (rows, cols)
    :param tile_shape: the number of images to tile (rows, cols)

    :param output_pixel_vals: if output should be pixel values (i.e. int8
    values) or floats

    :param scale_rows_to_unit_interval: if the values need to be scaled before
    being plotted to [0,1] or not


    :returns: array suitable for viewing as an image.
    (See:`Image.fromarray`.)
    :rtype: a 2-d array with same dtype as X.

    """

    assert len(img_shape) == 2
    assert len(tile_shape) == 2
    assert len(tile_spacing) == 2

    # The expression below can be re-written in a more C style as
    # follows :
    #
    # out_shape    = [0,0]
    # out_shape[0] = (img_shape[0]+tile_spacing[0])*tile_shape[0] -
    #                tile_spacing[0]
    # out_shape[1] = (img_shape[1]+tile_spacing[1])*tile_shape[1] -
    #                tile_spacing[1]
    out_shape = [
        (ishp + tsp) * tshp - tsp
        for ishp, tshp, tsp in zip(img_shape, tile_shape, tile_spacing)
    ]

    if isinstance(X, tuple):
        assert len(X) == 4
        # Create an output numpy ndarray to store the image
        if output_pixel_vals:
            out_array = np.zeros((out_shape[0], out_shape[1], 4),
                                    dtype='uint8')
        else:
            out_array = np.zeros((out_shape[0], out_shape[1], 4),
                                    dtype=X.dtype)

        #colors default to 0, alpha defaults to 1 (opaque)
        if output_pixel_vals:
            channel_defaults = [0, 0, 0, 255]
        else:
            channel_defaults = [0., 0., 0., 1.]

        for i in range(4):
            if X[i] is None:
                # if channel is None, fill it with zeros of the correct
                # dtype
                dt = out_array.dtype
                if output_pixel_vals:
                    dt = 'uint8'
                out_array[:, :, i] = np.zeros(
                    out_shape,
                    dtype=dt
                ) + channel_defaults[i]
            else:
                # use a recurrent call to compute the channel and store it
                # in the output
                out_array[:, :, i] = tile_raster_images(
                    X[i], img_shape, tile_shape, tile_spacing,
                    scale_rows_to_unit_interval, output_pixel_vals)
        return out_array

    else:
        # if we are dealing with only one channel
        H, W = img_shape
        Hs, Ws = tile_spacing

        # generate a matrix to store the output
        dt = X.dtype
        if output_pixel_vals:
            dt = 'uint8'
        out_array = np.zeros(out_shape, dtype=dt)

        for tile_row in range(tile_shape[0]):
            for tile_col in range(tile_shape[1]):
                if tile_row * tile_shape[1] + tile_col < X.shape[0]:
                    this_x = X[tile_row * tile_shape[1] + tile_col]
                    if scale_rows_to_unit_interval:
                        # if we should scale values to be between 0 and 1
                        # do this by calling the `scale_to_unit_interval`
                        # function
                        this_img = scale_to_unit_interval(
                            this_x.reshape(img_shape))
                    else:
                        this_img = this_x.reshape(img_shape)
                    # add the slice to the corresponding position in the
                    # output array
                    c = 1
                    if output_pixel_vals:
                        c = 255
                    out_array[
                        tile_row * (H + Hs): tile_row * (H + Hs) + H,
                        tile_col * (W + Ws): tile_col * (W + Ws) + W
                    ] = this_img * c
        return out_array

def test(model, sess, dataset, n_runs = 10):
    dataset = dataset
    n_batch = dataset.num_batch
    err = []
    for run in range(n_runs):
        dataset.reset()
        _err = []
        for i in range(n_batch):
            x,y = dataset.get_batch()
            _err.append(sess.run(model.err, {model.input:x, model.target:y}))
        _err = np.mean(_err)
        err.append(_err)

    err_mean = np.mean(err)
    err_var = np.var(err)
    sess.run(model.valid_err_assign,{model.valid_err_placeholder: err_mean}) # BUG 
    summary = sess.run(model.valid_err_summ)
    return err_mean, err_var, summary

def train(model, cfg, trainset, devset, MI_COMPUTED=False, RESTORED_FROM_THE_LAST_TRAINING = True, GREEDY_PIB_SAVE_WEIGHT=False):
    """train: validation
    """
    with open(os.path.join(cfg.logdir, 'cfg'), 'w') as f:
        pickle.dump(cfg, f)
    print(cfg.logdir)
    n_batch = trainset.num_batch
    max_iters = n_batch * cfg.max_num_epochs #max(n_batch * cfg.max_num_epochs, int(1e6)) 
    print('Max iter = %d'%(max_iters))
    valid_freq = cfg.valid_freq
    if valid_freq is None:
        valid_freq = n_batch

    display_freq = cfg.display_freq
    if display_freq is None:
        display_freq = n_batch
    
    save_freq = cfg.save_freq 
    if save_freq is None:
        save_freq = n_batch
    train_op = model.train_op
    if cfg.non_binary:
        n_runs = 1 
    else:
        n_runs = 10 

    # with model.graph.as_default() as graph:
    sess_cfg = tf.ConfigProto();
    sess_cfg.gpu_options.allow_growth = True
    train_writer = tf.summary.FileWriter(cfg.logdir + '/train')
    dev_err_writer = tf.summary.FileWriter(cfg.logdir + '/dev_err')
    train_err_writer = tf.summary.FileWriter(cfg.logdir + '/train_err')
    saver = tf.train.Saver()
    # sv = tf.train.Supervisor(logdir=cfg.logdir, 
    #                         global_step = model.global_step, 
    #                         summary_op=None,
    #                         init_op=model.init_op)
    with tf.Session(config=sess_cfg) as sess:
        sess.run(tf.global_variables_initializer())
        if RESTORED_FROM_THE_LAST_TRAINING:
            restore_path = os.path.join(cfg.logdir, 'lastest_model')
            if os.path.exists(os.path.join(cfg.logdir, 'lastest_model.index')):
                print("Restore from %s"%(restore_path))
                saver.restore(sess,restore_path )
            else:
                print('Not found the trained model! Traing from scratch ...')
    # with sv.managed_session(config = sess_cfg) as sess:
        last_step = sess.run(model.global_step)
        print('last_step = {}'.format(last_step))
        ep = last_step // n_batch
        best_ep = ep
        trainset.hard_reset()
        for _ in range(ep):
            trainset.reset()

        # Initial
        best_val_err, best_val_err_var,  dev_summ = test(model, sess, devset, n_runs)
        st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        print("[INITIAL] \n[{}]\nval/err: {}".format(st, best_val_err ))
        if MI_COMPUTED:
            save_file = os.path.join(cfg.logdir, 'mi_-1')
            monitor = model.exact_mi(sess,train_X, train_Y, save_file )
        # if cfg.VAL:
        #     train_err, _, train_summ = test(model, sess, trainset, n_runs) 
        # if cfg.VAL:
        #     print("[INITIAL] \n[{}]\ntrain/err: {}\nval/err: {}".format(st, train_err, best_val_err ))
        # else:
        #     print("[INITIAL] \n[{}]\nval/err: {}".format(st, best_val_err ))
        patience = 200 #50 
        j = 0 
        best_iter = np.inf
        _iter = last_step
        while (j < patience) and (_iter < max_iters):
            _iter += 1
            sys.stdout.write("\rProgress {}/{} ...   ".format(_iter, max_iters))
            sys.stdout.flush()
            # if sv.should_stop():
            #     break 
            last_step = sess.run(model.global_step)
            x,y = trainset.get_batch_by_bstep(last_step)
            feed_dict = {model.input:x, model.target:y}
            if cfg.model == 'pib':
                if (not cfg.non_binary) and (cfg.loss_type != 'nll'):
                    _, tmp_loss, step, train_summ, r_tensor_summ = sess.run([train_op, model.loss, model.global_step, model.train_loss_summ, model.r_tensor_summ], feed_dict)
                    train_writer.add_summary(r_tensor_summ, step)
                else:
                    _, tmp_loss, step, train_summ = sess.run([train_op, model.loss, model.global_step, model.train_loss_summ], feed_dict)
                train_writer.add_summary(train_summ, step)
                
            else:
                _, tmp_loss, step, train_summ = sess.run([train_op, model.loss, model.global_step, model.train_loss_summ], feed_dict)

            assert step == last_step + 1
            ep = (step - 1) // n_batch

            if step % display_freq == 0:
                st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                print("\n[{}]Epoch = {}, Step = {}, Loss = {}".format(st, ep, step, tmp_loss))

            if step % save_freq == 0:
                # sv.saver.save(sess, cfg.logdir + '/model_epoch_%d_step_%d'%(ep, step))
                saver.save(sess, os.path.join(cfg.logdir, 'lastest_model'))
            
            if MI_COMPUTED:
                if step % n_batch:
                    save_file = os.path.join(cfg.logdir, 'mi_-1')
                    monitor = model.exact_mi(sess,train_X, train_Y, save_file )

            if step % valid_freq == 0:
                dev_err_mean, dev_err_var,  dev_summ = test(model, sess, devset, n_runs) 
                dev_err_writer.add_summary(dev_summ, ep)
                if cfg.turn_on_validation_in_trainset:
                    train_err_mean, train_err_var, train_summ = test(model, sess, trainset, n_runs) 
                    train_err_writer.add_summary(train_summ, ep)

                if dev_err_mean < best_val_err:
                    best_iter = _iter
                    best_ep = ep 
                    best_val_err = dev_err_mean
                    best_val_err_var = dev_err_var 
                    print('saving best model ...')
                    saver.save(sess, os.path.join(cfg.bestdir,'model.best'))
                    j = 0
                    if GREEDY_PIB_SAVE_WEIGHT:
                        model.save(sess)
                else:
                    j += 1
                st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                if cfg.turn_on_validation_in_trainset:
                    print("\n[{}]\ntrain/err: {} ({})\nval/err: {} ({})".format(st, train_err_mean, train_err_var, dev_err_mean, dev_err_var))
                else:
                    print("\n[{}]\nval/err: {} ({})".format(st, dev_err_mean, dev_err_var))
        # saver.restore(sess, os.path.join(cfg.bestdir,'model.best'))
        st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        print("\n[{}]\n FINISHED, val/best_err: {} ({}), ep = {}, iter = {}".format(st, best_val_err, best_val_err_var, best_ep, best_iter))   
        return best_ep    

def full_train(model, cfg, trainset, devset, train_X = None, train_Y = None, RESTORED_FROM_THE_LAST_TRAINING = True, GREEDY_PIB_SAVE_WEIGHT=False, COMPUTE_AND_SAVE_MI=False):
    """full_train: Train using the best configuration found from validation. This utility trains a model until a pre-specified number of epoch

        Validation Hyperparameters:
            max_num_epochs 

        Functionality:
            Restore from the last training in case the training is unexpectedly shut down 
            Save the model weights at the maximum epoch 
    """
    print(cfg.logdir)
    n_batch = trainset.num_batch
    max_iters = n_batch * cfg.max_num_epochs

    valid_freq = cfg.valid_freq
    if valid_freq is None:
        valid_freq = n_batch

    display_freq = cfg.display_freq
    if display_freq is None:
        display_freq = n_batch
    
    save_freq = cfg.save_freq 
    if save_freq is None:
        save_freq = n_batch

    train_op = model.train_op
    if cfg.non_binary:
        n_runs = 1 
    else:
        n_runs = 10 

    sess_cfg = tf.ConfigProto();
    sess_cfg.gpu_options.allow_growth = True
    saver = tf.train.Saver()
    # sv = tf.train.Supervisor(logdir=cfg.logdir, 
    #                         global_step = model.global_step, 
    #                         summary_op=None,
    #                         init_op=model.init_op)
    with tf.Session(config=sess_cfg) as sess:
        sess.run(tf.global_variables_initializer())
        if RESTORED_FROM_THE_LAST_TRAINING:
            restore_path = os.path.join(cfg.logdir, 'lastest_model')
            if os.path.exists(os.path.join(cfg.logdir, 'lastest_model.index')):
                print("Restore from %s"%(restore_path))
                saver.restore(sess,restore_path )
            else:
                print('Not found the trained model! Training from scratch ...')
    # with sv.managed_session(config = sess_cfg) as sess:
        last_step = sess.run(model.global_step)
        print('last_step = {}'.format(last_step))
        ep = last_step // n_batch
        trainset.hard_reset()
        for _ in range(ep):
            trainset.reset()
        if COMPUTE_AND_SAVE_MI:
            save_file = os.path.join(cfg.saved_mi_dir, 'mi_-1')
            monitor = model.exact_mi(sess,train_X, train_Y, save_file )
        while ep <= cfg.max_num_epochs:
            sys.stdout.write("\rProgress {}/{} ...   ".format(last_step, max_iters))
            sys.stdout.flush()
            # if sv.should_stop():
            #     break 
            last_step = sess.run(model.global_step)
            x,y = trainset.get_batch_by_bstep(last_step)
            feed_dict = {model.input:x, model.target:y}
            _, tmp_loss, step = sess.run([train_op, model.loss, model.global_step], feed_dict)
            assert step == last_step + 1

            ep = (step - 1) // n_batch

            if step % display_freq == 0:
                st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                print("\n[{}]Epoch = {}, Step = {}, Loss = {}".format(st, ep, step, tmp_loss))
            if cfg.show_test_every_epoch:
                if (step - 1) % n_batch == 0:
                    dev_err_mean, dev_err_var,_ = test(model, sess, devset, n_runs)
                    print("\nEpoch = {}, Test error = {} ({})".format(ep, dev_err_mean, dev_err_var))
            if step % save_freq == 0:
                # sv.saver.save(sess, cfg.logdir + '/model_epoch_%d_step_%d'%(ep, step))
                saver.save(sess, os.path.join(cfg.logdir, 'lastest_model'))
            
            if COMPUTE_AND_SAVE_MI:
                if step % n_batch == 0:
                    save_file = os.path.join(cfg.saved_mi_dir, 'mi_%d'%(ep))
                    monitor = model.exact_mi(sess,train_X, train_Y, save_file )

        print("Saving the best model ...")
        saver.save(sess, os.path.join(cfg.logdir, 'lastest_model'))
        if GREEDY_PIB_SAVE_WEIGHT:
            print('Saving the model weights ...')
            model.save(sess)
        print("Running the best model on the test set ...")
        dev_err_mean, dev_err_var,  _ = test(model, sess, devset, n_runs)
        st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        print("\n[{}]\nval/err: {} ({})".format(st, dev_err_mean, dev_err_var))
        return dev_err_mean

def test_multimodal(model, sess, upper_input, lower_input, gen_dir, epoch):
    """test_multimodal: generating the lower half of a MNIST image given the upper half
    """
    num_modes = model.n_samples ** model.n_layers
    gen_output = np_sigmoid(sess.run(model.output, {model.input: upper_input}))
    gen_output = np.reshape(gen_output, (-1, num_modes, model.out_dim))
    gen_ave = np.mean(gen_output, axis=1)
    
    complete_input = np.hstack((upper_input, lower_input)) # (10, 784)
    blind = np.ones((10,392), dtype='uint8')
    blind_input = np.hstack((upper_input, blind))
    gen_data = np.zeros((29 * 10 + 1, 347), dtype='uint8')

    for i in range(10):
        upper_part = np.transpose(np.repeat( upper_input[i].reshape(-1,1), 10, axis=1)) # (10, 392)
        lower_part = np.transpose(np.repeat( lower_input[i].reshape(-1,1), 10, axis=1)) # (10, 392)
        sampling_ind = np.random.randint(low=0, high = num_modes, size= 9).tolist()

        # print(gen_output[sampling_ind,i,:].shape,  gen_ave[i].reshape )
        merged = np.hstack((upper_part, np.vstack((gen_output[i, sampling_ind,:], gen_ave[i].reshape((1,-1))) ))) # (10, 784)
        # print(complete_input[i].reshape((1,-1)).shape, blind_input[i].reshape((1,-1)).shape, merged.shape)
        merged = np.vstack((complete_input[i].reshape((1,-1)), blind_input[i].reshape((1,-1)), merged) ) # (12, 784)

        gen_data[29*i +1: 29*(i+1),:] = tile_raster_images(
            X=merged,
            img_shape=(28, 28),
            tile_shape=(1, 12),
            tile_spacing=(1, 1)
        )
    
    gen_name = 'gen_at_epoch_%i.png'% epoch 
    print('Saving gen at %s'%(gen_name))
    gen_data = Image.fromarray(gen_data)
    gen_data.save(os.path.join(gen_dir, gen_name))

def test_tfd(model, sess, face_in_unique, face_in , face_out, gen_dir, epoch):
    """test_tfd: Generate faces with (hopefully) different facial expressions for a subject given the mean image of that subject 

    **Features:
        - compute test log-probability 
    """
    num_modes = model.n_samples ** model.n_layers
    gen_output_unique = sess.run(model.output, {model.input: face_in_unique})
    if model.config.output_structure == 'multivariate_normal':
        gen_output, sigma = sess.run([model.output, model.sigma], {model.input: face_in})
    elif model.config.output_structure == 'multivariate_ber':
        gen_output = sess.run(model.output, {model.input: face_in})
    gen_output = np_sigmoid(gen_output)
    gen_output = np.reshape(gen_output, (-1, num_modes, model.out_dim))
    gen_output_unique = np_sigmoid(gen_output_unique)
    gen_output_unique = np.reshape(gen_output_unique, (-1, num_modes, model.out_dim))
    # compute log-probability 
    logprob = 0
    if model.config.output_structure == 'multivariate_normal':
        sigma_rshp = np.expand_dims(np.expand_dims(sigma, 0), 0)
        face_out_rshp = np.expand_dims(face_out, 1)

        # logprob = np.sum(np.log(np.mean(np.exp(np.sum(-0.5 * (face_out_rshp - gen_output) ** 2 / sigma_rshp ** 2, -1)), axis=1)))
        # - ( 0.5 * model.in_dim * np.log(2* np.pi) + 0.5 * np.sum(np.log( sigma ** 2  )) )*face_in.shape[0]  
        logprob = -np.mean(
                    np.sum(-0.5 * ((face_out_rshp - gen_output) ** 2) / (sigma_rshp ** 2) - 0.5 * np.log( sigma_rshp ** 2), axis=-1) - 0.5 * model.in_dim * np.log( 2 * np.pi), 
                ) 
        # print('logprob = ', logprob)

    NUM_NORMAL_SAMPLES = 1
    if model.config.output_structure == 'multivariate_normal':
        NUM_NORMAL_SAMPLES = 10
        epsilon = np.random.multivariate_normal(np.zeros(model.out_dim), np.diag(np.ones(model.out_dim)), size= gen_output_unique.shape[:-1] + (NUM_NORMAL_SAMPLES,))
        gen_output_unique = np.expand_dims(gen_output_unique,2)  + (sigma ** 2) * epsilon
        gen_output_unique = np.reshape( gen_output_unique, (gen_output_unique.shape[0], -1, model.out_dim ))

    # gen_output = sess.run(model.output, {model.input: face_in}) # use Gaussian mean as output 

    # gen_ave = np.mean(gen_output, axis=1)

    img_size = int(np.sqrt( model.config.in_dim  ))

    particle_range = range(num_modes * NUM_NORMAL_SAMPLES)
    np.random.shuffle(particle_range)
    
    NUM_SUBJECT_TO_VIS = 10
    NUM_SAMPLES = 14
    gen_data = np.zeros( (NUM_SUBJECT_TO_VIS * (1 + NUM_SAMPLES), model.config.in_dim  )  )
    for i in range(NUM_SUBJECT_TO_VIS):
        gen_data[ i * (1 + NUM_SAMPLES)  ] = face_in_unique[i] # select the first 'NUM_SUBJECT_TO_VIS' subjects in 'face_in' 
        rnd_idx = particle_range[:NUM_SAMPLES]  #np.random.randint(low = 0, high = num_modes * NUM_NORMAL_SAMPLES, size=NUM_SAMPLES).tolist()
        gen_data[ i * (1 + NUM_SAMPLES) + 1 : (i+1) * (1 + NUM_SAMPLES) ] = gen_output_unique[i, rnd_idx, :]  

    gen_data = tile_raster_images(
        X = gen_data, 
        img_shape = (img_size, img_size), 
        tile_shape = (NUM_SUBJECT_TO_VIS, NUM_SAMPLES + 1),
        tile_spacing = (1,1)
    )

    gen_name = 'gen_at_epoch_%i.png'% epoch 
    print('Saving gen at %s'%(gen_name))
    gen_data = Image.fromarray(gen_data)
    gen_data.save(os.path.join(gen_dir, gen_name))

    return gen_output_unique, logprob

def train_multimodal(model, test_model, cfg, trainset):
    """train: validation
    """
    with open(os.path.join(cfg.logdir, 'cfg'), 'w') as f:
        pickle.dump(cfg, f)
    n_batch = trainset.num_batch
    max_iters = n_batch * cfg.max_num_epochs#max(n_batch * cfg.max_num_epochs, int(1e6)) 
    valid_freq = cfg.valid_freq
    if valid_freq is None:
        valid_freq = n_batch
    display_freq = cfg.display_freq
    if display_freq is None:
        display_freq = valid_freq
    save_freq = cfg.save_freq
    if save_freq is None:
        save_freq = valid_freq
    train_op = model.train_op


    # with model.graph.as_default() as graph:
    sess_cfg = tf.ConfigProto();
    sess_cfg.gpu_options.allow_growth = True
    train_writer = tf.summary.FileWriter(cfg.logdir + '/train')
    saver = tf.train.Saver()
    sv = tf.train.Supervisor(logdir=cfg.logdir, 
                            global_step = model.global_step, 
                            summary_op=None,
                            init_op=model.init_op)

    with sv.managed_session(config = sess_cfg) as sess:
        last_step = sess.run(model.global_step)
        print('last_step = {}'.format(last_step))
        ep = last_step // n_batch
        for _ in range(ep):
            trainset.reset()

        # Initial
        logprob_list = []
        if test_model.config.dataset == 'tfd':
            # test_tfd(test_model, sess, tfd_face_expr_X_test_unique, cfg.gen_dir, -1)
            print('testing tfd...')
            _, logprob = test_tfd(test_model, sess, tfd_face_expr_X_test_unique, tfd_face_expr_X_test, tfd_face_expr_Y_test, cfg.gen_dir, ep-1)
            logprob_list.append(logprob)
            print('logprob = %.9f'%(logprob))
        elif test_model.config.dataset == 'mnist':
            test_multimodal(test_model, sess, upper_half_test, lower_half_test, cfg.gen_dir, ep-1)
        else:
            raise ValueError('Unrecognized dataset!')
        patience = 50 
        j = 0 
        best_iter = np.inf
        _iter = last_step
        while (j < patience) and (_iter < max_iters):
            _iter += 1
            sys.stdout.write("\rProgress {}/{} ...   ".format(_iter, max_iters))
            sys.stdout.flush()
            if sv.should_stop():
                break 
            last_step = sess.run(model.global_step)
            x,y = trainset.get_batch_by_bstep(last_step)
            feed_dict = {model.input:x, model.target:y}
            if cfg.model == 'pib':
                _, tmp_loss, step, train_summ, r_tensor_summ = sess.run([train_op, model.loss, model.global_step, model.train_loss_summ, model.r_tensor_summ], feed_dict)
                train_writer.add_summary(train_summ, step)
                train_writer.add_summary(r_tensor_summ, step)
            else:
                _, tmp_loss, step, train_summ = sess.run([train_op, model.loss, model.global_step, model.train_loss_summ], feed_dict)

            assert step == last_step + 1
            ep = (step - 1) // n_batch

            if step % display_freq == 0:
                st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                print("\n[{}]Epoch = {}, Step = {}, Loss = {}".format(st, ep, step, tmp_loss))

            if step % save_freq == 0:
                sv.saver.save(sess, cfg.logdir + '/model_epoch_%d_step_%d'%(ep, step))
            if step % valid_freq == 0:
                if test_model.config.dataset == 'tfd':
                    # test_tfd(test_model, sess, tfd_face_expr_X_test_unique, cfg.gen_dir, ep)
                    _ ,logprob = test_tfd(test_model, sess, tfd_face_expr_X_test_unique, tfd_face_expr_X_test, tfd_face_expr_Y_test, cfg.gen_dir, ep)
                    logprob_list.append(logprob)
                    print('logprob = %.9f'%(logprob))
                    np.save( os.path.join(model.config.logdir, 'test_logprob'), np.asarray(logprob_list) )

                elif test_model.config.dataset == 'mnist':
                    test_multimodal(test_model, sess, upper_half_test, lower_half_test, cfg.gen_dir, ep)
                else:
                    raise ValueError('Unrecognized dataset!')