from config import cfg
from model import * 
from utils import * 
from utils import _makedir
from data_load import * 
import tensorflow as tf 
import os 
import numpy as np

# NETWORK 
cfg.in_dim = 12
cfg.out_dim = 1 
cfg.hidden_dims = [10,8,6,4]#[1024, 1024]
cfg.output_structure = 'univariate_ber' 
#'multivariate_ber', 'univariate_ber', 'categorical', 'multivariate_normal'
cfg.activation = 'sigmoid'

# IF STOCHASTICITY 
cfg.non_binary = False #False

# LOSS TYPE 
cfg.loss_type = 'pib' #nll, vcr, pib 

# STOCHASTICITY HYPERPARAMETERS
cfg.train_n_samples = [32,16,8,4]
cfg.test_n_samples = [32,16,8,4]
cfg.beta = 1e-4
cfg.estimator = 'Raiko'

# RANDOMIZATION 
cfg.seed = 3333

# TRAINING 
cfg.logdir = _makedir('./log/sphere12/official/nlayers_4/nepoch100k/pib/beta4/sgd/lr1/seed3333')
cfg.gen_dir = _makedir(os.path.join(cfg.logdir, 'gen'))
cfg.max_num_epochs = 100000
cfg.optimizer_arg = {'adadelta':{'learning_rate':0.01, 'rho': 0.95, 'epsilon':1e-6}, 
            'adam':{'learning_rate':1e-3, 'beta1':0.9, 'beta2':0.999, 'epsilon':1e-8},
            'gradientdescent':{'learning_rate':1},
            'adagrad':{'learning_rate':0.1}}
cfg.optimizer = 'gradientdescent'
cfg.bestdir = _makedir(os.path.join(cfg.logdir, 'best'))
cfg.display_freq = None 
cfg.save_freq = None
cfg.batch_size = 32 #16
cfg.test_batch_size = 100 #16
cfg.valid_freq = None

data_dir = './data/sphere12'
train_X = np.load(os.path.join(data_dir, 'sphere12_train_X.npy'))
train_Y = np.load(os.path.join(data_dir, 'sphere12_train_Y.npy'))
train_Y = np.expand_dims(train_Y[:,1].astype(np.float32), 1)
test_X = np.load(os.path.join(data_dir, 'sphere12_test_X.npy'))
test_Y = np.load(os.path.join(data_dir, 'sphere12_test_Y.npy'))
test_Y = np.expand_dims(test_Y[:,1].astype(np.float32), 1)

##########################################
trainset = DataSet(train_X, train_Y, cfg.batch_size, shuffle=True)
trainset_val = DataSet(train_X, train_Y, cfg.batch_size, shuffle=False)
testset = DataSet(test_X, test_Y, cfg.batch_size, shuffle=False)

input = tf.placeholder(tf.float32, [None, cfg.in_dim], 'input')
train_model = pib(input, cfg)
test_model = pib(input, cfg, is_train=False)

##########################################

batch_size = trainset.batch_size
n_batch = trainset.num_batch
max_iters = n_batch * cfg.max_num_epochs
valid_freq = cfg.valid_freq
if valid_freq is None:
    valid_freq = n_batch
    
dislay_freq = cfg.display_freq
if dislay_freq is None:
    display_freq = n_batch

save_freq = cfg.save_freq 
if save_freq is None:
    save_freq = n_batch
    
train_op = train_model.train_op

sess_cfg = tf.ConfigProto(device_count = {'GPU': 2});
sess_cfg.gpu_options.allow_growth = True
saver = tf.train.Saver()
# sv = tf.train.Supervisor(logdir=cfg.logdir, 
#                         global_step = model.global_step, 
#                         summary_op=None,
#                         init_op=model.init_op)

train_writer = tf.summary.FileWriter(cfg.logdir + '/train')
dev_err_writer = tf.summary.FileWriter(cfg.logdir + '/dev_err')
train_err_writer = tf.summary.FileWriter(cfg.logdir + '/train_err')

saver = tf.train.Saver()
sess = tf.InteractiveSession(config=sess_cfg)
# sess.run(train_model.init_op)
sess.run(tf.global_variables_initializer())
RESTORED_FROM_THE_LAST_TRAINING = True 
if RESTORED_FROM_THE_LAST_TRAINING:
    restore_path = os.path.join(cfg.logdir, 'lastest_model')
    if os.path.exists(os.path.join(cfg.logdir, 'lastest_model.index')):
        print("Restore from %s"%(restore_path))
        saver.restore(sess,restore_path )
    else:
        print('Not found the trained model! Traing from scratch ...')

test_err, _, _ = test(train_model, sess, testset, n_runs=1)
train_err, _, _ = test(train_model, sess, trainset_val, n_runs=1)
# st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
print("Epoch = INIT, Train Err = {}, Test Err = {}".format(train_err, test_err))


# with sv.managed_session(config = sess_cfg) as sess:
save_file = os.path.join(cfg.logdir, 'mi_-1')
monitor = train_model.exact_mi(sess,train_X, train_Y, save_file )
last_step = sess.run(train_model.global_step)
print('last_step = {}'.format(last_step))
ep = last_step // n_batch
for _ in range(ep):
    trainset.reset()
while ep <= cfg.max_num_epochs:
    sys.stdout.write("\rProgress {}/{} ...   ".format(last_step, max_iters))
    sys.stdout.flush()
#     if sv.should_stop():
#         break 
    last_step = sess.run(train_model.global_step)
    x,y = trainset.get_batch_by_bstep(last_step)
#     y = np.argmax(y,1).astype(np.int32) % 2
#     y = np.expand_dims(y.astype(np.float32), 1)
    feed_dict = {train_model.input:x, train_model.target:y}
    _, tmp_loss, step, train_summ = sess.run([train_op, train_model.loss, train_model.global_step, train_model.train_loss_summ], feed_dict)
    train_writer.add_summary(train_summ, step)
    assert step == last_step + 1

    ep = (step - 1) // n_batch

    if step % display_freq == 0:
#         st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        print("\nEpoch = {}, Step = {}, Loss = {}".format(ep, step, tmp_loss))
    if step % valid_freq == 0:
        test_err, _, test_err_summ = test(train_model, sess, testset, n_runs=1)
        train_err, _, train_err_summ = test(train_model, sess, trainset_val, n_runs=1)
        train_err_writer.add_summary(train_err_summ, ep)
        dev_err_writer.add_summary(test_err_summ, ep)
        saver.save(sess, os.path.join(cfg.logdir, 'lastest_model'))
#         st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        print("Epoch = {}, Step = {}, Train Err = {}, Test Err = {}".format(ep, step, train_err, test_err))
    if step % n_batch == 0:
        save_file = os.path.join(cfg.logdir, 'mi_%d'%(ep))
        monitor = train_model.exact_mi(sess,train_X, train_Y, save_file )
